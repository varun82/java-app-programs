package com.numberoperations;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        int choice;
        Scanner input_scanner = new Scanner(System.in);
        NumberOperations operation = new NumberOperations();

        operation.generateNumbers();

        while(true){
            System.out.println("Enter your Choice :: ");
            System.out.println("1.  Check all of them even or not. ");
            System.out.println("2.  How many are multiples of 5 ");
            System.out.println("3.  How many greater than 50 ");
            System.out.println("4.  Exit ");

            choice = input_scanner.nextInt();

            switch (choice)
            {
                case 1:
                    operation.allAreEvenOrNot();
                    break;

                case 2:
                    operation.multipleOfFive();
                    break;

                case 3:
                    operation.geraterThan50();
                    break;

                case 4:
                    System.out.println("Exit Sucessfully ... ! ");
                    System.exit(0);

                default:
                    System.out.println("Wrong Choice, Try again ... ! ");
            }
        }
    }
}
