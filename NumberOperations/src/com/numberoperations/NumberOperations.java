package com.numberoperations;

import java.util.Scanner;

public class NumberOperations {
    int numbersArray[] = new int[11];
    int evenCount=0;
    int multipleCount = 0;
    int graterThan50Count=0;

    Scanner input_scanner = new Scanner(System.in);

    public void generateNumbers(){

        System.out.println("Enter 10 Numbers to proceed :: ");

        for ( int number = 1; number< numbersArray.length ; number++){
            System.out.println("Enter "+ number + " Number :: ");
            int num = input_scanner.nextInt();
            numbersArray[number] = num;
        }

        System.out.println("Numbers are :: ");

        for ( int number = 1; number<numbersArray.length ; number++){
            System.out.println(numbersArray[number]);
        }
    }

    public void allAreEvenOrNot(){
        for(int number=1;number<numbersArray.length;number++)
        {
            if((numbersArray[number] & 1) == 0){
                evenCount++;
            }
        }

        if( evenCount ==10 ){
            System.out.println("All numbers are even.");
        }
        else{
            System.out.println(    evenCount + " numbers are even from numbers list.");
        }
    }

    public void multipleOfFive(){
        for(int number=1;number<numbersArray.length;number++)
        {
            if((numbersArray[number] % 5 ) == 0){
                multipleCount++;
            }
        }

        System.out.println(   multipleCount + " numbers are multiple of 5 from numbers list.");
    }

    public void geraterThan50(){
        for(int number=1;number<numbersArray.length;number++)
        {
            if(numbersArray[number] > 50 ){
                graterThan50Count++;
            }
        }

        if(graterThan50Count==0){
            System.out.println(" All numbers are below 50... ! ");
        }
        else
        {
            System.out.println(   graterThan50Count + " numbers are greater than 50 from numbers list.");
        }
    }
}
