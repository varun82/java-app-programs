package com.numberoperations;
import java.util.Scanner;
public class CountOfEvenOdd {

    int numbersArray[] = new int[9];
    int evenSum=0;
    int oddSum=0;


    Scanner input_scanner = new Scanner(System.in);

    public void generateNumbers(){

        System.out.println("Enter 10 Numbers to proceed :: ");

        for ( int number = 1; number< numbersArray.length ; number++){
            System.out.println("Enter "+ number + " Number :: ");
            int num = input_scanner.nextInt();
            numbersArray[number] = num;
        }

        System.out.println("Numbers are :: ");

        for ( int number = 1; number<numbersArray.length ; number++){
            System.out.println(numbersArray[number]);
        }
    }

    public void getEvenIndexSum(){
        for ( int number = 1; number<numbersArray.length ; number++){
           if( numbersArray[number] % 2 != 0){
               evenSum = evenSum + numbersArray[number];
           }
           else{
               oddSum = oddSum + numbersArray[number];
           }
        }

        if ( oddSum == evenSum){
            System.out.println("True");
        }
        else{
            System.out.println("False");
        }

    }

}
